<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1" user-scalable=no”>
    <title>Vision</title>
    <link rel="stylesheet" type="text/css" href="dist/css/app.css">
    <link rel="stylesheet" href="https://cdn.plyr.io/2.0.11/plyr.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container">

    <div class="row">

        <div class="col-md-7">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <h1>User ID</h1>
                    <label for="id">User ID:</label>
                    <input type="text" name="userId" class="form-control" id="id" style="width: 100px;" required>
                </div>

                <div class="form-group">
                    <h1>Programmes</h1>
                    <label for="id">Programme ID 1:</label>
                    <input type="text" name="programme1" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 2:</label>
                    <input type="text" name="programme2" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 3:</label>
                    <input type="text" name="programme3" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 4:</label>
                    <input type="text" name="programme4" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 5:</label>
                    <input type="text" name="programme5" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 6:</label>
                    <input type="text" name="programme6" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 7:</label>
                    <input type="text" name="programme7" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 8:</label>
                    <input type="text" name="programme8" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 9:</label>
                    <input type="text" name="programme9" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="id">Programme ID 10:</label>
                    <input type="text" name="programme10" class="form-control" id="id" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                </div>


            </form>
        </div>
    </div>
</div>

</body>
</html>